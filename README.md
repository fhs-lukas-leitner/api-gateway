# API-Gateway
Source: https://github.com/Java-Techie-jt/spring-cloud-gatway-hystrix

Team: Lisa Maria Panzl, Lukas Leitner<br>
Please contact us, if anything is not clear.

Over this service you will get your requests. <br>
Each service must be registered at the discovery-server. Please read the discovery-server README. <br>
Please define also an application name, like following: <br>

```
# application.properties or application.yml
spring.application.name = <your-service-name>
```

After that, the api-gateway admin (Lisa or Lukas) must add your service name to the api-gateway configuration.<br>


## Port

In the api-gateway ``application.yml``:<br>
Use the port ``9090`` for debugging and <br>
``9091`` in the production.

## Routes
New services will get an own api-route which will redirect any request. <br>
The following code is a part of our api-config, which  <br>contains your
service-name ``uri: lb://<your-service-name>``  <br>and the path for your
``@RequestMapping(..)`` in your ``RestController``
<br>
```
# api-gateway application.yml
spring:
  cloud:
    gateway:
      routes:
        - id: songService
          uri: lb://songservice
          predicates:
          - Path=/api/songService/**
        ...
        - id: demo-client
          uri: lb://client-demo-registration
          predicates:
            - Path=/api/client-demo-registration/**
```
#### Important for your:<br>
Please write for ``@RequestMapping(..)`` ``<your-application-name>/`` as parameter.<br>
`It is your entrypoint for all requests

```
# a demo service...
# client-demo-registration\src\main\java\sst\alpha\cdr\CdrApplication.java
...

@SpringBootApplication
@EnableDiscoveryClient
public class CdrApplication {

    public static void main(String[] args) {
        SpringApplication.run(CdrApplication.class, args);
    }

}

# please write for RequestMapping '<your-application-name>/'
# It is your entrypoint for all requests
@RequestMapping("client-demo-registration") 
@RestController
class ServiceInstanceRestController {

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/app/{applicationName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(
            @PathVariable String applicationName) {
        return this.discoveryClient.getInstances(applicationName);
    }
}

```
