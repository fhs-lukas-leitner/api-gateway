package sst.alpha.apigateway.filters;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import sst.alpha.apigateway.services.JwtService;


import java.util.List;

@Component
public class JwtGatewayFilterFactory extends AbstractGatewayFilterFactory<JwtGatewayFilterFactory.Config> {

    Logger logger = LoggerFactory.getLogger(JwtGatewayFilterFactory.class);

    public static class Config {
    }

    private JwtService jwtService;

    @Autowired
    public JwtGatewayFilterFactory(JwtService jwtService) {
        super(Config.class);
        this.jwtService = jwtService;
    }

    @Override
    public GatewayFilter apply(Config config) {
        /*
         * Important to understand: This filter is a workaround.
         * You are not using OAuth flow - just JWT!
         */
        return (exchange, chain) -> {
            // TODO: Improve error handling (Response Code currently 500)
            List<String> authHeaders = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION);
            if (authHeaders == null || authHeaders.isEmpty()) {
                logger.error("Log: Missing Authorization Header");
                throw new ResponseStatusException(HttpStatus.NON_AUTHORITATIVE_INFORMATION, "Missing Authorization Header");
            }
            String authHeader = authHeaders.get(0);
            if (authHeader == null || !authHeader.startsWith("Bearer ")) {
                logger.error("Log: Missing Authorization Header");
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Authorization header invalid (expecting bearer token)");
            }
            String jwt = authHeader.substring(7);
            try {
                Jws<Claims> result  = jwtService.parseAndValidate(jwt); // throws Exception, if token is invalid
            }
            catch(Exception exp) {
                logger.error("Token validation error. Type: "+ exp.getClass() + "\n Message: " + exp.getMessage() );
                throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Token validation error", exp);
            }

            /* TODO:
             * You could also use RestTemplate to call the validate-Method of the user service here instead of using jwtService.
             * If possible, validate the token yourself using the public key.
             */
            return chain.filter(exchange);
        };
    }

}
